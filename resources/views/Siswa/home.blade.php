@extends('layouts.siswa.dashboard')

@section('body')
<div class="container my-3 d-flex">
    <div class="card" style="background-color:#EEEDED">
        <div class="card-body">
            @if (Auth()->user()->status == null)
            <center>
                <h4>Anda belum melakukan proses Prakerin Apapun </h4>
                <a href="/lanjut" class="btn btn-danger btn-sm">Mulai</a>
            </center>
            @elseif (Auth()->user()->status == 'Menunggu Proses Validasi Surat Pengantar')
            <h4>Anda sedang di tahap {{ Auth()->User()->status }}</h4>

            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <a href="/download/{{ Auth()->User()->pengantar_pkl }}" class="btn btn-success text-white">Download Surat Pengantar</a>
                </div>
            </div>

            @elseif (Auth()->user()->status == 'Memilih Tempat Prakerin')
            <h3>Lembar Pengesahan anda sudah di validasi dan di terima oleh pembimbing</h3>
            <h3>silahkan Pilih Perusahaan untuk melakukan Prakerin</h3>

            <p>Pesan dari pembimbing:</p><textarea readonly cols="30" rows="3">{{ Auth()->user()->ket_message_pengantar }}</textarea><br>
            <a href="/download/{{ Auth()->User()->pengantar_pkl }}" class="btn btn-warning btn-xm">Lihat Surat pengantar</a>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Pilih Tempat Prakerin</button>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Pilih Tempat PKL</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/insertTempatPkl" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ Auth()->User()->id }}">
                                <input type="hidden" name="status" value="Mengajukan Surat Pengantar ke Perusahaan">
                                <select class="custom-select" id="inputGroupSelect04" name="perusahaan_id">
                                    <option selected>Choose...</option>
                                    @foreach ($perusahaan as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_perusahaan }}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @elseif (Auth()->user()->status == 'Mengajukan Surat Pengantar ke Perusahaan')
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Silahkan Cetak dan Kirim Surat Pengantar ke Tempat Prakerin yang telah anda pilih</h3>
                    <h4 class="card-title">Nama Perusahaan yang dipilih : {{ Auth()->User()->perusahaan->nama_perusahaan }}</h4>
                    <h4 class="card-title">Alamat Perusahaan yang dipilih : {{ Auth()->User()->perusahaan->alamat_perusahaan }}</h4>
                    <a href="/download/{{ Auth()->User()->pengantar_pkl }}" class="btn btn-primary">Cetak Surat Pengantar</a>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <p class="card-text">Jika Anda Sudah Mengirimkan Surat Pengantar Klik Tombol dibawah ini!</p>
                    <form action="/gantiStatus" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ Auth()->User()->id }}">
                        <input type="hidden" name="status" value="Masa Sanggah balasan surat Pengantar/pegajuan">
                        <input type="submit" class="btn btn-success" value="Saya Sudah Mengirimkan Surat Pengantar ke Perusahaan tempat Prakerin">
                    </form>
                </div>
            </div>
            @elseif (Auth()->user()->status == 'Masa Sanggah balasan surat Pengantar/pegajuan')
            <div class="card">
                <div class="card-body">
                    <h4>Anda Sedang dalam Masa sanggah Menunggu balasan surat pengantar/pengajuan dari perusahaan {{ Auth()->User()->perusahaan->nama_perusahaan }}</h4>
                    <label for="">Tekan tombol dibawah jika menerima surat balasan pengajuan dari perusahaan yang diajukan</label><br>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Konfirmasi Surat balasan pengajuan</button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/konfirmasiPengajuan" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
                                        <div class="mb-3">
                                            <label for="" class="form-label">NIS</label>
                                            <input type="text" class="form-control" name="" id="" aria-describedby="helpId" value="{{ Auth()->User()->nis }}" readonly>
                                        </div>
                                        <div class="mb-3">
                                            <label for="" class="form-label">Nama Lengkap</label>
                                            <input type="text" class="form-control" name="" id="" aria-describedby="helpId" value="{{ Auth()->User()->name }}" readonly>
                                        </div>
                                        <div class="mb-3">
                                            <label for="" class="form-label">Perusahaan yang di ajukan</label>
                                            <input type="text" class="form-control" name="" id="" aria-describedby="helpId" value="{{ Auth()->User()->perusahaan->nama_perusahaan }}" readonly>
                                        </div>
                                        <div class="mb-3">
                                            <label for="" class="form-label">Konfirmasi Balasan Dari perusahaan yang diajukan </label>
                                            <select class="form-control" name="status" id="" style="height: 30px" required>
                                                <option value=""></option>
                                                <option value="Pengajuan Prakerin Disetujui">Disetujui</option>
                                                <option value="Pengajuan Prakerin Ditolak">Ditolak</option>
                                            </select>
                                            <div class="mb-3">
                                                <label for="" class="form-label">Mulai PKL</label>
                                                <input type="date" class="form-control" name="mulai_pkl" id="" aria-describedby="helpId" value="{{ Auth()->User()->perusahaan->nama_perusahaan }}">
                                            </div>
                                            <div class="mb-3">
                                                <label for="" class="form-label">Selesai PKL</label>
                                                <input type="date" class="form-control" name="selesai_pkl" id="" aria-describedby="helpId" value="{{ Auth()->User()->perusahaan->nama_perusahaan }}">
                                            </div>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            @elseif (Auth()->user()->status == 'Pengajuan Prakerin Disetujui')
            <h4>Pengajuan Prakerin disetujui</h4>
            {{-- <h5>Nama Pembimbing Internal : {{ Auth()->user()->pembimbing->name }}</h5> --}}
            <h5>Admin Sedang memilih pembimbing untuk anda</h5>
            <h5>Mulai melakukan Prakerin pada tanggal: {{ Auth()->user()->mulai_pkl }} - {{ Auth()->user()->selesai_pkl }}</h5>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                Baca Tata Tertib Prakerin!
            </button>
            <a href="/home" class="btn btn-danger bi bi-arrow-left"> Kembali</a>

            <!-- Modal -->
            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Tata tertib Prakerin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            TATA TERTIB PKL/Prakerin SMK MUHAMMADIYAH LOA KULU TAHUN 2021
                            Tata Tertib Prakerin disusun sebagai pedoman siswa Prakerin untuk dapat berbuat, bertindak dan berperilaku demi kelancaran pelaksanaan dan keberhasilan Prakerin di DU/DI.
                            Tata tertib ini mengatur kegiatan siswa saat prapelaksanaan dan selama pelaksanaan di lokasi Prakerin.

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Saya Sudah membaca</button>
                        </div>
                    </div>
                </div>
                @elseif (Auth()->user()->status == 'Siap melakukan sidang Prakerin')
                    Pengajuan Sidang Prakerin Sedang di ajukan!
                @elseif (Auth()->user()->status == 'Sudah Mendapatkan Pembimbing')
                <h3>Admin sudah menetapkan pembimbing untuk anda</h3>
                <h5>Silahkan Mulai PKL Pada tanggal : {{ Auth()->User()->mulai_pkl }} - {{ Auth()->User()->selesai_pkl }}</h5>
                <h5>Nama Pembimbing : {{ Auth()->User()->pembimbing->name }}</h5>
                <a href="/jurnal" class="btn btn-outline-dark">Isi Jurnal Harian</a>
                @elseif (Auth()->user()->status == 'Pengajuan Prakerin Ditolak')
                <div class="alert alert-danger" role="alert">
                    Pengajuan Prakerin di {{ Auth()->user()->perusahaan->nama_perusahaan }} <strong>DITOLAK</strong>
                </div>
                <h4>Silahkan membuat kembali Surat pengantar / pengajuan baru</h4>
                <form action="/gantiStatus" method="post">
                    @csrf
                    <input type="hidden" name="status" value="">
                    <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
                    <input type="submit" value="Buat Surat Pengantar/ pengajuan baru">
                </form>
                @elseif (Auth()->user()->status == 'Laporan Siap')
                    Selamat Laporan Anda sudah selesai!<br>
                    1 langkah lagi untuk melakukan sidang Prakerin<br>
                    Saat ini pembimbing sedang meng input nilai anda<br>
                @elseif (Auth()->user()->status == 'Nilai Sudah di input') 
                    Selamat Pembimbing sudah memberi nilai akhir untuk anda!<br>
                    Anda bisa melihat nilai tersebut di profile anda<br>
                    Tekan tombol dibawah untuk mengajukan sidang Prakerin<br>
                    <form action="/gantiStatus" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ Auth()->User()->id}}">
                        <input type="hidden" name="status" value="Siap melakukan sidang Prakerin">
                        <input type="submit" class="btn btn-outline-dark mt-2" value="Saya siap untuk Sidang Prakerin">
                    </form>
                @elseif (Auth()->User()->status == 'Lembar pengesahan di tolak')
                    Lembar pengesahan ditolak
                    Silahkan Perbaiki lembar pengesahan
                <form action="/upload/pengantar" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <div class="mb-3">
                            <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
                            <input type="hidden" name="status" value="Menunggu Proses Validasi Surat Pengantar">
                            <label for="" class="form-label">Silahkan ajukan surat pengajuan / pengantar Prakerin</label>
                            <input type="file" class="form-control" name="file" id="" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <input type="submit" value="Upload" class="btn btn-primary">
                </form>
                @elseif (Auth()->user()->laporan->keterangan == 'Laporan Siap di cetak' && Auth()->User()->status == 's')
                    Tekan tombol dibawah jika anda sudah siap untuk melakukan sidang
                    <form action="/gantiStatus" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ Auth()->User()->id}}">
                        <input type="hidden" name="status" value="Siap melakukan sidang Prakerin">
                        <input type="submit" value="Saya siap untuk Sidang Prakerin">
                    </form>
                @elseif (Auth()->user()->status == 'Siap melakukan sidang Prakerin')
                    @foreach ($data as $item)
                    @if ($item)
                        Admin Sudah menetapkan Sidang Prakerin pada Tanggal {{ $item->tanggal }}<br>
                        Silahkan untuk menyiapkan PPT untuk sidang Prakerin
                    @else
                        Tanggal Sidang sedang di proses oleh pembimbing<br>
                        Anda bisa menyiapkan PPT untuk Sidang Prakerin<br>        
                    @endif
                    @endforeach
                @endif
                </div>
            </div>
        </div>
    <a href="/dashboard/siswa" class="btn btn-danger btn-sm bi bi-arrow-left"> Kembali</a>
</div>
@endsection