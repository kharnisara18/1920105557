@extends('layouts.siswa.dashboard')

@section('body')
{{-- <div class="container mt-2">
    <h6><a href="/dashboard/siswa" class="bi bi-desktop" style="text-decoration: none"> Dashboard Siswa</a> > Halaman Penyusunan Laporan</h6>
</div> --}}

<div class="container mt-2">
    @if (Auth()->User()->laporan_id == null)
    <div class="card">
        <div class="card-header bg-danger text-white text-center">
            <h3>Penyusunan Laporan Prakerin</h3>
        </div>
        <div class="card-body">
            <h4>Silahkan susun laporan mulai dari sekarang</h4>
            <form action="/createLaporan" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{ Auth()->User()->id }}">
                <input type="hidden" name="nis" value="{{ Auth()->User()->nis }}">
                <input type="hidden" name="keterangan" value="Perlu di validasi">
                <div class="mb-3">
                    <label for="" class="form-label">Upload Laporan Prakerin</label>

                    <input type="file" class="form-control" name="file_laporan" id="" aria-describedby="helpId" placeholder="">
                    <small id="helpId" class="form-text text-muted">Laporan yang di upload akan segera di validasi oleh pembimbing</small>
                </div>
                <button type="submit" class="btn btn-success bi bi-upload" style="float: left;"> Submit</button>
            </form>
            <button type="button" class="btn btn-outline-primary" style="float: right" data-toggle="modal" data-target="#exampleModalCenter">
                Baca Tata cara Penyusunan Laporan
            </button>
            @foreach ($data as $item)
            <a href="/download/{{ $item->file_laporan }}" class="btn btn-outline-info " style="float: right">Download Contoh Laporan Prakerin</a>
            @endforeach
        </div>
    </div>
    @elseif (Auth()->User()->laporan->keterangan == "Perlu di Revisi")
    <div class="card">
        <div class="card-header bg-danger text-white text-center">Revisi Laporan penyusunan Prakerin</div>
        <div class="card-body">
            <h4>Pembimbing telah mengecek laporan yang anda upload</h4>
            <h5>Laporan yang anda upload perlu di <strong>Revisi</strong></h5><br>
            <div class="mb-3">
                <label for="" class="form-label">Catatan Dari Pembimbing</label>
                <textarea class="form-control" name="" id="" rows="3" readonly>{{ Auth()->User()->laporan->coment }}</textarea>
            </div>
            <a href="/download/{{ Auth()->User()->laporan->file_laporan }}" class="btn btn-info">Download Laporan Prakerin </a>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadLaporan" data-whatever="@mdo">Upload Laporan Baru</button>
            <br>
            <div class="modal fade" id="uploadLaporan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/updateLaporan" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ Auth()->User()->laporan->id }}">
                                <input type="hidden" name="keterangan" value="Perlu di validasi">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Upload Laporan:</label>
                                    <input type="file" class="form-control" id="recipient-name" name="file_laporan">
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @foreach ($data as $item)
            <a href="/download/{{ $item->file_laporan }}" class="btn btn-outline-info " style="float: right">Download Contoh Laporan Prakerin</a>
            @endforeach
            <button class="btn btn-primary mt-2">Baca Tata cara pembuatan Laporan</button>
        </div>
    </div>
    @elseif (Auth()->User()->laporan->keterangan == 'Laporan Siap di cetak')
    <div class="card" style="border: none">
            Silahkan cetak / printout Laporan Prakerin anda Lalu ajukan tanda tangan ke perusahaan dan sekolah<br>
            Pesan Dari Pembimbing : <br>
            <textarea name="" id="" cols="30" rows="10">{{ Auth()->User()->laporan->coment }}</textarea><br>
            <a href="/download/{{ Auth()->User()->laporan->file_laporan }}" class="btn btn-primary">Download Laporan Prakerin</a><br>
            Jika laporan sudah mendapatkan tanda tangan dari perusahaan dan sekolah silahkan klik tombol dibawah <br>
            <form action="/updateKetLaporan" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ Auth()->User()->laporan->id}}">
                <input type="hidden" name="user_id" value="{{ Auth()->User()->id}}">
                <input type="hidden" name="keterangan" value="Laporan sudah di tanda tangan">
                <input type="hidden" name="status" value="Laporan Siap">
                <input type="submit" value="Laporan sudah di tanda tangan oleh pihak perusahaan dan pihak sekolah">
            </form>
    </div>
    @elseif (Auth()->User()->laporan->keterangan == 'Perlu di validasi')
    <div class="card">
        <div class="card-header bg-danger text-white text-center">Menunggu validasi laporan Prakerin</div>
        <div class="card-body">
            <h5>Laporan Anda Berhasil Dikirim!</h5>
            Tunggu laporan di validasi oleh pembimbing<br>
            <a href="https://api.whatsapp.com/send/?phone={{ Auth()->User()->pembimbing->no_telp }}&text=Halo+nama+saya+{{ Auth()->User()->name }}&app_absent=0" target="_blank">Hubungi Pembimbing</a>
        </div>
    </div>
    @elseif (Auth()->User()->laporan->keterangan == 'Laporan sudah di tanda tangan')
    <div class="card">
        <div class="card-header bg-success text-white text-center">Success</div>
        <div class="card-body">
            <h5>Laporan sudah di tanda tangan!</h5>
            Laporan sudah di tanda tangan oleh pihak perusahaan dan pihak sekolah<br>
            <a href="/download/{{ Auth()->User()->laporan->file_laporan }}" class="btn btn-primary mt-2">Download Laporan Prakerin</a><br>
        </div>
    </div>
    @else
    <div class="card">
        <div class="card-header bg-danger text-white text-center">Menunggu validasi laporan Prakerin</div>
        <div class="card-body">
            <h5>Laporan Anda Berhasil Dikirim!</h5>
            Tunggu laporan di validasi oleh pembimbing<br>
        </div>
    </div>
    @endif
    <a href="/dashboard/siswa" class="btn btn-danger bi bi-arrow-left mt-3"></a>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tata cara menyusun laporan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @foreach ($data as $item)
                <textarea name="" id="" cols="30" rows="10">{{ $item->keterangan }}</textarea>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection