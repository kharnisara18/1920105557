@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card my-3">
                <div class="card-body">
                    <h4 style="padding-top: 2px; ">Selamat Datang {{ Auth()->user()->name }} </h4>
                    <form action="/upload/pengantar" method="POST" enctype="multipart/form-data">
                        @csrf
        
                        <div class="form-group">
                            <div class="mb-3">
                                <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
                                <input type="hidden" name="status" value="Menunggu Proses Validasi Surat Pengantar">
                                <label for="" class="form-label">Silahkan ajukan surat pengajuan / pengantar Prakerin</label>
                                <input type="file" class="form-control" name="file" id="" aria-describedby="helpId" placeholder="" required>
                            </div>
                        </div>
        
                        <button type="submit" class="btn btn-primary bi bi-upload"> Upload</button>
                        <a href="/dashboard/siswa/pengajuan_pkl" class="btn btn-danger bi bi-arrow-left"> Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection