<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Register Admin | Sistem Informasi Prakerin</title>

</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark" style="background: #0590D8;">
            <div class="container">
            <a href="#" class="navbar-brand">
                <img src="/img/logolagi.png" alt="" class="rounded-circle" style="width: 50px; margin-right: 4px">
                <strong>
                SMK Al Falah
                </strong>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
                <div class="collapse flex justify-content-end navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="#" class="nav-link text-white mr-2"><i class="fas fa-home"></i> Home</a></li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="#" class="nav-link text-white mr-2"><i class="far fa-address-card"></i> About</a></li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="#" class="nav-link text-white mr-2"><i class="fas fa-phone-alt"></i> Contact</a></li>
                    </ul>
                </div>
            </div>
    </nav>
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>

</html>