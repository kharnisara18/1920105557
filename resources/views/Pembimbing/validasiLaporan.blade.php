
@extends('layouts.pembimbing.dashboard')

@section('body')

<div class="row mt-2">
    <div class="col-lg-12">
        <form action="/validasiLaporan" method="POST">
            @csrf
            @foreach ($data as $item)
                <input type="hidden" name="id" value="{{ $item->laporan->id }}">
    
                <div class="form-group">
                    <label for="" class="form-label">Nama Siswa :</label>
                    <input type="text" class="form-control" value="{{ $item->name }}" id="" aria-describedby="helpId" placeholder="" readonly>
    
                    <label for="recipient-name" class="col-form-label">Keterangan :</label>
                    <select class="form-control" name="keterangan" id="">
                        <option value="">--pilih keterangan--</option>
                        <option value="Perlu di Revisi">Perlu di Revisi</option>
                        <option value="Laporan Siap di cetak">Laporan Siap di cetak</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Message :</label>
                    <textarea class="form-control" id="message-text" name="coment"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/cekLaporan" class="btn btn-danger bi bi-arrow-left"></a>
            @endforeach
        </form>
    </div>
</div>
    

@endsection